This project is created with [Angular CLI](https://angular.io/cli).
We have typescript support in this project using [typescript](https://code.visualstudio.com/docs/typescript/typescript-compiling) with [webpack-cli](https://webpack.js.org/guides/installation/).

Press  `Cmd + Shift + V`  form your Mac keyboard to preview the README.md.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode. Then, visit http://localhost:4200 through a browser window to run the application.

## Learn More

You can learn more in the [Angular Directives](https://angular.io/api/core/Directive).

### Simple Angular Directives Setup In An Angular Project

**Initialize the project with `NPM`.** *Make sure you have a stable version of* [NodeJS](https://nodejs.org/en/), [NPM](https://www.npmjs.com/) & [Angular CLI](https://angular.io/cli) *is installed in your system globally.*

```bash
ng new angularDirectives
```

Answer some questions asked by the `Angular CLI` when initiating the new project.

**Check** the `app.component.html` file to understand the directives. Visit [app.component.html](https://bitbucket.org/abhisekdutta507/angular-directives/src/master/src/app/app.component.html). Please visit [Directive](https://angular.io/guide/attribute-directives#directives-overview) for more info.

**Description** - There are three kinds of **directives** in Angular:

1. **Components** — directives with a template.

2. **Structural directives** — change the DOM layout by adding and removing DOM elements. Two examples are `NgFor` and `NgIf`.

3. **Attribute directives** — change the appearance or behavior of an element, component, or another directive. `NgClass` and `NgStyle` are built-in attribute directives.

### Let's build some directives

`*ngIf` directive. Visit [*ngIf](https://angular.io/api/common/NgIf) for more info.

```html
<!--
    1. *ngIf is a structural directive
-->
<div *ngIf="commands.length; then listOfCommands else noCommands"></div>

<ng-template #listOfCommands>
    ...
</ng-template>

<ng-template #noCommands>
    ...
</ng-template>
```

`*ngForOf` directive. Visit [*ngForOf](https://angular.io/api/common/NgForOf) for more info.

```html
<!--
    1. *ngForOf is a structural directive
-->
<li
    *ngFor="let command of commands; index as i; even as isEvent; odd as isOdd; first as isFirst; last as isLast"
    class="card card-small"
    [ngClass]="{'card-even' : isEvent, 'card-odd' : isOdd, 'card-first' : isFirst, 'card-last' : isLast}"
    (click)="selection.value = command.value"
    [tabindex]="i">
    ...
</li>
```

`*ngSwitchCase` directive. Visit [*ngSwitchCase](https://angular.io/api/common/NgSwitch) for more info.

```html
<!--
    1. [ngSwitch] is an attribute directive
    2. *ngSwitchCase is a structural directive
    3. *ngSwitchDefault is also a structural directive
-->
<div class="terminal" [ngSwitch]="selection.value">
    <pre *ngSwitchDefault>ng generate component xyz</pre>
    <pre *ngSwitchCase="'directive'">ng g directive def</pre>
    ...
</div>
```

`[ngClass]` directive. Visit [[ngClass]](https://angular.io/api/common/NgClass) for more info.

```html
<!--
    1. [ngClass] is a built-in attribute directive
-->
<div [ngClass]="{'card-even' : isEvent, 'card-odd' : isOdd, 'card-first' : isFirst}">
    ...
</div>
```

`Custom attribute` directive. Visit [Custom attribute directive](https://angular.io/guide/attribute-directives#build-a-simple-attribute-directive) for more info.

```typescript
import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[poweredBy]'
})
export class ColourfulUiDirective {
  @Input() poweredBy: string;

  constructor(private el: ElementRef) {}

  ngOnInit() {
    const { nativeElement } = this.el;
    nativeElement.textContent += ` - rendered by ${this.poweredBy} custom directive.`;
  }

}
```

**Usage** of `custom attribute` directive.
```html
<h2 [poweredBy]="'Angular'">List of commands</h2>
```
OR
```html
<h2 poweredBy> ... </h2>
```

Hurray!! we have learned the basic concepts of Angular directives.