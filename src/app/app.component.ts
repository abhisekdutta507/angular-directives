import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-directives';

  public poweredBy: string = 'Angular';

  commands = [
    {
      label: 'New Component',
      value: 'component'
    },
    {
      label: 'Add Custom Directive',
      value: 'directive'
    },
    {
      label: 'Angular Material',
      value: 'material'
    },
    {
      label: 'Add PWA Support',
      value: 'pwa'
    },
    {
      label: 'Add Dependency',
      value: 'dependency'
    },
    {
      label: 'Run and Watch Tests',
      value: 'test'
    },
    {
      label: 'Build for Production',
      value: 'build'
    }
  ];

  selection = {
    value: ''
  }
}
