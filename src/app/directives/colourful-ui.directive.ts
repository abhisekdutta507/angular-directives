import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[poweredBy]'
})
export class ColourfulUiDirective {
  @Input() poweredBy: string;

  constructor(private el: ElementRef) {}

  ngOnInit() {
    const { nativeElement } = this.el;
    nativeElement.textContent += ` - rendered by ${this.poweredBy} custom directive.`;
  }

}
